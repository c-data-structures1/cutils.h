/**
 * Copyright (C) 2020 Fabio Sussarellu
 *
 * This file is part of CUtils.h.
 *
 * CUtils.h is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CUtils.h is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CUtils.h.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CUTILS_H
#define CUTILS_H

// * DNT
#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef UINT_DEF
#define UINT_DEF
#endif // ! UINT_DEF
typedef unsigned int uint;
#ifndef ITEM_DEF
#define ITEM_DEF
typedef void *Item;
#endif // ! ITEM_DEF
// * EDNT

void pressToContinue();
FILE *fileRead(char *filename);
bool equalStrings(char *a, char *b);
FILE *smartFopen(char *filename, char *mode);
unsigned int countNumberOfLines(char *filename);
bool checkFilestream(FILE *stream);
int alphabeticOrder(char *a, char *b);

#endif // ! CUTILS_H
