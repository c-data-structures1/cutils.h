/**
 * Copyright (C) 2020 Fabio Sussarellu
 *
 * This file is part of CUtils.h.
 *
 * CUtils.h is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CUtils.h is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CUtils.h.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CUtils.h"

/**
 * Check if a came first than b in alphabetical order and return
 * -1 : a < b
 * 0 : a == b
 * 1 : a > b
 */
int alphabeticOrder(char *a, char *b) {
    uint LengthA = strlen(a);
    uint LengthB = strlen(b);
    uint Length  = LengthA > LengthB ? LengthB : LengthA;

    char A, B;
    for (uint i = 0; i < Length; i++) {
        A = toupper(a[i]);
        B = toupper(b[i]);
        if (A < B) { return -1; }
        if (A > B) { return 1; }
    }

    if (LengthA < LengthB) {
        return -1;
    } else if (LengthA > LengthB) {
        return 1;
    } else {
        return 0;
    }
}

/**
 * Return true if filestream it's not NULL
 */
bool checkFilestream(FILE *stream) { return stream != NULL; }

// Count number of rows inside a file
unsigned int countNumberOfLines(char *filename) {
    FILE *stream       = smartFopen(filename, "r");
    unsigned int lines = 0;
    char c, precedente;
    precedente = EOF;
    for (c = getc(stream); c != EOF; precedente = c, c = getc(stream))
        if (c == '\n') { lines++; }
    lines += precedente != '\n' ? 1 : 0;
    fclose(stream);
    return lines;
}

/**
 * Return true if two strings are equals
 */
bool equalStrings(char *a, char *b) {
    unsigned int Length = strlen(a);
    if (strlen(b) != Length) { return false; }
    for (unsigned int i = 0; i < Length; i++) {
        if (a[i] != b[i]) { return false; }
    }
    return true;
}

/**
 * Open a filestream but only if file exist
 */
FILE *fileRead(char *filename) { return smartFopen(filename, "r"); }

// Print "Press enter to continue..."
void pressToContinue() {
    puts("Press enter to continue...");
    getchar();
    getchar();
}

/**
 * Safely open a filestream, exit program if it's not possible
 */
FILE *smartFopen(char *filename, char *mode) {
    FILE *f = fopen(filename, mode);
    if (checkFilestream(f)) { return f; }
    printf("Error opening file %s\n", filename);
    exit(1);
    return NULL;
}